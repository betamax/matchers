0.2.0 - 2015-06-17
------------------

- Add ``MultipartFormDataBodyMatcher``

0.1.0 - 2014-08-20
------------------

- Add ``JSONBodyMatcher``

- Add ``URLEncodedBodyMatcher``
